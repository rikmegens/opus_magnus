== 01-Virtualization

=== Installing the components


Install the packages in the RHEL 8 virtualization module:
[source, text]
----
# yum module install virt
----

Install the virt-install and virt-viewer packages
[source, text]
----
# yum install virt-install virt-viewer
----

Start the libvirtd service.
[source, text]
----
# systemctl start libvirtd
----

Verify that your system is prepared to be a virtualization host:
[source, text]
----
# virt-host-validate
[...]
QEMU: Checking for device assignment IOMMU support         : PASS
QEMU: Checking if IOMMU is enabled by kernel               : WARN (IOMMU appears to be disabled in kernel. Add intel_iommu=on to kernel cmdline arguments)
LXC: Checking for Linux >= 2.6.26                          : PASS
[...]
LXC: Checking for cgroup 'blkio' controller mount-point    : PASS
LXC: Checking if device /sys/fs/fuse/connections exists    : FAIL (Load the 'fuse' module to enable /proc/ overrides)
----

If all virt-host-validate checks return a PASS value, your system is prepared for creating VMs.

If any of the checks return a FAIL value, follow the displayed instructions to fix the problem.

If any of the checks return a WARN value, consider following the displayed instructions to improve virtualization capabilities.


You can find the entire documentation on RHEL KVM virtualization here:
https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/configuring_and_managing_virtualization/getting-started-with-virtualization-in-rhel-8_configuring-and-managing-virtualization#enabling-virtualization-in-rhel8_virt-getting-started[] 